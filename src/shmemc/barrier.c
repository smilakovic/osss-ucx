/* For license: see LICENSE file at top-level */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif /* HAVE_CONFIG_H */

#include "shmemc.h"
#include "shmemu.h"
#include "state.h"
#include "memfence.h"


#include "shmem/defs.h"

#define BARRIER_LINEAR 1
#define BARRIER_TREE 2
#define BARRIER_DISSEMINATION 3

int barrier_algorithm = BARRIER_LINEAR;

/*
 * Degree that's used for tree barrier implementation.
 */
int tree_degree = 2;

inline static int
get_children_info(int tree_size, int tree_degree, int node,
                  int *children_begin, int *children_end) {
    *children_begin = node * tree_degree + 1;

    if (*children_begin >= tree_size) {
        *children_begin = *children_end = -1;
        return 0;
    }

    *children_end = *children_begin + tree_degree;

    if (*children_end > tree_size) {
        *children_end = tree_size;
    }

    return *children_end - *children_begin;
}

/*
 * just play with a simple linear barrier for now
 */

inline static void
barrier_sync_helper_linear(int start, int log2stride, int size, long *pSync)
{
    const int me = proc.rank;
    const int stride = 1 << log2stride;

    if (start == me) {
        const int npokes = size - 1;
        long poke = *pSync + 1;
        int pe;
        int i;

        /* wait for the rest of the AS to poke me */
        shmemc_wait_eq_until64(pSync, npokes + SHMEM_SYNC_VALUE);
        *pSync = SHMEM_SYNC_VALUE;

        /* send acks out */
        pe = start + stride;
        for (i = 1; i < size; i += 1) {
            shmemc_put(pSync, &poke, sizeof(poke), pe);
            pe += stride;
        }
    }
    else {
        /* poke root */
        shmemc_inc64(pSync, start);

        /* get ack */
        shmemc_wait_ne_until64(pSync, SHMEM_SYNC_VALUE);
        *pSync = SHMEM_SYNC_VALUE;
    }
}

/*
 * Tree barrier implementation
 */

inline static void
barrier_sync_helper_tree(int start, int log2stride, int size, long *pSync)
{
    const int me = proc.rank;
    const int stride = 1 << log2stride;

    // Get my index in the active set
    const int me_as = (me - start) / stride;

    // Calculate parent's index in the active set
    int parent_idx = me_as != 0 ? (me_as - 1) / tree_degree : -1;

    // Get information about children
    int children_begin, children_end;
    int num_children = get_children_info(size, tree_degree, me_as,
                                         &children_begin, &children_end);

    // Wait for pokes from the children
    long npokes = num_children;
    if (npokes != 0) {
        shmemc_wait_eq_until64(pSync, SHMEM_SYNC_VALUE + npokes);
    }

    if (parent_idx != -1) {
        // Poke the parent exists
        long ret = shmemc_finc64(pSync, start + parent_idx * stride);

        // Wait for the poke from parent
        shmemc_wait_eq_until64(pSync, SHMEM_SYNC_VALUE + npokes + 1);
    }

    // Clear pSync and poke the children
    *pSync = SHMEM_SYNC_VALUE;
    for (int child = children_begin; child != children_end; child++) {
        shmemc_inc64(pSync, start + child * stride);
    }
}

/*
 * Dissemination barrier implementation
 */

inline static void
barrier_sync_helper_dissemination(int start, int log2stride, int size, long *pSync)
{
    const int me = proc.rank;
    const int stride = 1 << log2stride;

    // Calculate my index in the active set
    const int me_as = (me - start) / stride;

    for (int round = 0, distance = 1; distance < size; round++, distance <<= 1) {
        int target_as = (me_as + distance) % size;

        // Poke the target for the current round
        shmemc_inc64(&pSync[round], start + target_as * stride);

        // Wait until poked in this round
        shmemc_wait_ne_until64(&pSync[round], SHMEM_SYNC_VALUE);

        // Reset pSync element, fadd is used instead of add because we have to
        // be sure that reset happens before next invocation of barrier
        shmemc_fadd64(&pSync[round], -1, me);
    }
}


/*
 * Proxy method for barrier implementation
 */
inline static void 
barrier_sync_helper(int start, int log2stride, int size, long *pSync) {
    switch (barrier_algorithm) {
    case BARRIER_LINEAR:
        barrier_sync_helper_linear(start, log2stride, size, pSync);
        break;
    case BARRIER_TREE:
        barrier_sync_helper_tree(start, log2stride, size, pSync);
        break;
    case BARRIER_DISSEMINATION:
        barrier_sync_helper_dissemination(start, log2stride, size, pSync);
        break;
    default:
        logger(LOG_BARRIER, "barrier_algorithm has invalid value."
                            " Using linear barrier algorithm");
        barrier_sync_helper_linear(start, log2stride, size, pSync);
    }
}
    

/*
 * internal psyncs
 */
long shmemc_all_barrier[SHMEM_BARRIER_SYNC_SIZE] = {SHMEM_SYNC_VALUE};
long shmemc_all_sync[SHMEM_BARRIER_SYNC_SIZE] = {SHMEM_SYNC_VALUE};

/*
 * API
 */

void
shmemc_barrier(int start, int log2stride, int size, long *pSync)
{
    shmemc_quiet();

    barrier_sync_helper(start, log2stride, size, pSync);
}

void
shmemc_barrier_all(void)
{
    shmemc_barrier(0, 0, proc.nranks, shmemc_all_barrier);
}

void
shmemc_sync(int start, int log2stride, int size, long *pSync)
{
    LOAD_STORE_FENCE();

    barrier_sync_helper(start, log2stride, size, pSync);
}

void
shmemc_sync_all(void)
{
    shmemc_sync(0, 0, proc.nranks, shmemc_all_sync);
}
