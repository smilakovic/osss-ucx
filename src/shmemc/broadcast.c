/* F

eifdef HAVE_CONFIG_H
# include "config.h"
#endif /* HAVE_CONFIG_H */

#include "shmemc.h"
#include "shmemu.h"
#include "state.h"

#include <sys/types.h>

#define BROADCAST_LINEAR 1
#define BROADCAST_TREE 2
#define BROADCAST_BINOMIAL 3
int broadcast_algorithm = 1;

/*
 * Degree that's used for tree barrier implementation.
 */
int broadcast_tree_degree = 2;

inline static int
get_children_info(int tree_size, int tree_degree, int node,
                  int *children_begin, int *children_end) {
    *children_begin = node * tree_degree + 1;

    if (*children_begin >= tree_size) {
        *children_begin = *children_end = -1;
        return 0;
    }

    *children_end = *children_begin + tree_degree;

    if (*children_end > tree_size) {
        *children_end = tree_size;
    }

    return *children_end - *children_begin;
}

inline static int
get_parent_binomial(int node) {
    return node == 0 ? -1 : node & (node - 1);
}

inline static int
get_children_info_binomial(int tree_size, int node, int *children) {
    int children_num = 0;

    // Lowest bit
    int mask;
    if (node == 0) {
        mask = 1 << (sizeof(int) * 8 - 2);
    } else {
        mask = node & ~(node - 1);
        mask >>= 1;
    }

    while (mask != 0) {
        int child = node | mask;
        if (child < tree_size) {
            children[children_num++] = child;
        }

        mask >>= 1;
    }

    return children_num;
}


/*
 * stupid linear broadcast during development
 */

void
broadcast_helper_linear(void *target, const void *source, size_t nbytes, 
                        int PE_root, int PE_start, 
                        int logPE_stride, int PE_size, long *pSync)
{
    const int stride = 1 << logPE_stride;
    const int root = (PE_root * stride) + PE_start;
    const int me = proc.rank; 

    shmemc_barrier(PE_start, logPE_stride, PE_size, pSync);
    if (me != root) {
        shmemc_get(target, source, nbytes, root);
    }
}

/*
 * Broadcast tree implemenation.
 * In the virtual tree, tree root and broadcast root are swapped.
 */
 
void
broadcast_helper_tree(void *target, const void *source, size_t nbytes, 
                        int PE_root, int PE_start, 
                        int logPE_stride, int PE_size, long *pSync)
{
    const int me = proc.rank;
    const int stride = 1 << logPE_stride;

    // Get my index in the active set
    int me_as = (me - PE_start) / stride;

    // Get my index after tree root and broadcast root swap
    if (me_as == PE_root) {
        me_as = 0;
    } else if (me_as == 0) {
        me_as = PE_root;
    }

    // Get information about children
    int children_begin, children_end;
    int num_children = get_children_info(PE_size, broadcast_tree_degree, me_as,
                                         &children_begin, &children_end);

    // Wait for the data form the parent
    if (me_as != 0) {
        shmemc_wait_ne_until64(pSync, SHMEM_SYNC_VALUE);
        source = target;
        // Send ack
        int parent = (me_as - 1) / broadcast_tree_degree;
        if (parent == PE_root) {
            parent = 0;
        } else if (parent == 0) {
            parent = PE_root;
        }
        shmemc_inc64(pSync, PE_start + parent * stride);
    }
    
    // Send data to children
    if (num_children != 0) {
        for (int child = children_begin; child != children_end; child++) {
            int dst = PE_start + (child == PE_root ? 0 : child) * stride;
            shmemc_put(target, source, nbytes, dst);
        }

        shmemc_fence();

        for (int child = children_begin; child != children_end; child++) {
            int dst = PE_start + (child == PE_root ? 0 : child) * stride;
            shmemc_inc64(pSync, dst);
        }

        shmemc_wait_eq_until64(pSync, SHMEM_SYNC_VALUE + num_children +
                                (me_as == 0 ? 0 : 1));
    }
    
    *pSync = SHMEM_SYNC_VALUE;
}

void
broadcast_helper_binomial(void *target, const void *source, size_t nbytes, 
                          int PE_root, int PE_start, 
                          int logPE_stride, int PE_size, long *pSync)
{
    static int children[sizeof(int) * 8];
    const int me = proc.rank;
    const int stride = 1 << logPE_stride;

    // Get my index in the active set
    int me_as = (me - PE_start) / stride;

    // Get my index after tree root and broadcast root swap
    if (me_as == PE_root) {
        me_as = 0;
    } else if (me_as == 0) {
        me_as = PE_root;
    }

    // Get information about children
    int num_children = get_children_info_binomial(PE_size, me_as, children);

    // Wait for the data form the parent
    if (me_as != 0) {
        shmemc_wait_ne_until64(pSync, SHMEM_SYNC_VALUE);
        source = target;
        // Send ack
        int parent = get_parent_binomial(me_as);
        if (parent == PE_root) {
            parent = 0;
        } else if (parent == 0) {
            parent = PE_root;
        }
        shmemc_inc64(pSync, PE_start + parent * stride);
    }
    
    // Send data to children
    if (num_children != 0) {
        for (int i = 0; i < num_children; i++) {
            int dst = PE_start + (children[i] == PE_root ? 0 : children[i]) * stride;
            shmemc_put(target, source, nbytes, dst);
            shmemc_fence();
            shmemc_inc64(pSync, dst);
        }

        shmemc_wait_eq_until64(pSync, SHMEM_SYNC_VALUE + num_children +
                                (me_as == 0 ? 0 : 1));
    }
    
    *pSync = SHMEM_SYNC_VALUE;
}

void
broadcast_helper_binomial2(void *target, const void *source, size_t nbytes, 
                          int PE_root, int PE_start, 
                          int logPE_stride, int PE_size, long *pSync)
{
    static int children[sizeof(int) * 8];
    const int me = proc.rank;
    const int stride = 1 << logPE_stride;

    // Get my index in the active set
    int me_as = (me - PE_start) / stride;

    // Get my index after tree root and broadcast root swap
    if (me_as == PE_root) {
        me_as = 0;
    } else if (me_as == 0) {
        me_as = PE_root;
    }

    // Get information about children
    int num_children = get_children_info_binomial(PE_size, me_as, children);

    // Wait for the data form the parent
    if (me_as != 0) {
        shmemc_wait_ne_until64(pSync, SHMEM_SYNC_VALUE);
        source = target;
        // Send ack
        int parent = get_parent_binomial(me_as);
        if (parent == PE_root) {
            parent = 0;
        } else if (parent == 0) {
            parent = PE_root;
        }
        shmemc_inc64(pSync, PE_start + parent * stride);
    }
    
    // Send data to children
    if (num_children != 0) {
        for (int i = 0; i < num_children; i++) {
            int dst = PE_start + (children[i] == PE_root ? 0 : children[i]) * stride;
            shmemc_put(target, source, nbytes, dst);
        }

        shmemc_fence();

        for (int i = 0; i < num_children; i++) {
            int dst = PE_start + (children[i] == PE_root ? 0 : children[i]) * stride;
            shmemc_inc64(pSync, dst);
        }

        shmemc_wait_eq_until64(pSync, SHMEM_SYNC_VALUE + num_children +
                                (me_as == 0 ? 0 : 1));
    }
    
    *pSync = SHMEM_SYNC_VALUE;
}

#define SHMEMC_BROADCAST_TYPE(_name, _size)                             \
    void                                                                \
    shmemc_broadcast##_name(void *target, const void *source,           \
                            size_t nelems,                              \
                            int PE_root, int PE_start,                  \
                            int logPE_stride, int PE_size,              \
                            long *pSync)                                \
    {                                                                   \
        switch (broadcast_algorithm) {                                  \
        case BROADCAST_LINEAR:                                          \
            broadcast_helper_linear(target, source, _size * nelems,     \
                                    PE_root, PE_start, logPE_stride,    \
                                    PE_size, pSync);                    \
            break;                                                      \
        case BROADCAST_TREE:                                            \
            broadcast_helper_tree(target, source, _size * nelems,       \
                                  PE_root, PE_start, logPE_stride,      \
                                  PE_size, pSync);                      \
            break;                                                      \
        case BROADCAST_BINOMIAL:                                        \
            broadcast_helper_binomial(target, source, _size * nelems,   \
                                      PE_root, PE_start, logPE_stride,  \
                                      PE_size, pSync);                  \
            break;                                                      \
        case 4:                                        \
            broadcast_helper_binomial2(target, source, _size * nelems,   \
                                      PE_root, PE_start, logPE_stride,  \
                                      PE_size, pSync);                  \
            break;                                                      \
        default:                                                        \
            logger(LOG_BARRIER, "broadcast_algorithm has invalid value."\
                                " Using linear broadcast algorithm");   \
            broadcast_helper_linear(target, source, _size * nelems,     \
                                    PE_root, PE_start, logPE_stride,    \
                                    PE_size, pSync);                    \
        }                                                               \
    }                                                                   \

SHMEMC_BROADCAST_TYPE(32, 4)

SHMEMC_BROADCAST_TYPE(64, 8)

