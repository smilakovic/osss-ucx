/* For license: see LICENSE file at top-level */

#ifndef _SHMEMC_READENV_H
#define _SHMEMC_READENV_H 1

void read_environment(void);

#endif /* ! _SHMEMC_READENV_H */
